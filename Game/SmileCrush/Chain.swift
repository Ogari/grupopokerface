//
//  Chain.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import Foundation

class Chain: Hashable, Printable {
    var smiles = [Smile]()
    
    var score = 0
    
    enum ChainType: Printable {
        case Horizontal
        case Vertical
        
        var description: String {
            switch self {
            case .Horizontal: return "Horizontal"
            case .Vertical: return "Vertical"
            }
        }
    }
    
    var chainType: ChainType
    
    init(chainType: ChainType) {
        self.chainType = chainType
    }
    
    func add(smile: Smile) {
        smiles.append(smile)
    }
    
    func first() -> Smile {
        return smiles[0]
    }
    
    func last() -> Smile {
        return smiles[smiles.count - 1]
    }
    
    var length: Int {
        return smiles.count
    }
    
    var description: String {
        return "type:\(chainType) smiles:\(smiles)"
    }
    
    var hashValue: Int {
        return reduce(smiles, 0) { $0.hashValue ^ $1.hashValue }
    }
}

func ==(lhs: Chain, rhs: Chain) -> Bool {
    return lhs.smiles == rhs.smiles
}