//
//  GameScene.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var level: Level!
    
    let TileWidthOriginal:CGFloat = 32.0
    let TileHeightOriginal:CGFloat = 36.0
    
    let TileWidth: CGFloat = adjustX(32.0)
    let TileHeight: CGFloat = adjustY(36.0)
    
    // Layers
    let gameLayer = SKNode()
    let tilesLayer = SKNode()
    let smilesLayer = SKNode()
    let cropLayer = SKCropNode()
    let maskLayer = SKNode()
    
    // Selected Sprites
    var selectionSprite = SKSpriteNode()
    
    // Swipe
    var swipeHandler: ((Swap) -> ())?
    
    var swipeFromColumn: Int?
    var swipeFromRow: Int?
    
    // Sounds
    
    let swapSound = SKAction.playSoundFileNamed("Chomp.wav", waitForCompletion: false)
    let invalidSwapSound = SKAction.playSoundFileNamed("Error.wav", waitForCompletion: false)
    let matchSound = SKAction.playSoundFileNamed("Ka-Ching.wav", waitForCompletion: false)
    let fallingSound = SKAction.playSoundFileNamed("Scrape.wav", waitForCompletion: false)
    let addSound = SKAction.playSoundFileNamed("Drip.wav", waitForCompletion: false)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) is not used in this app")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        anchorPoint = CGPoint(x: 0.5, y: 0)
        
        // Background
        let background = SKSpriteNode(imageNamed: "Background")
        background.setScale(self.size.width / 320)
        background.position = CGPoint(x: 0, y: (TileHeightOriginal * 6))
        addChild(background)
        
        // Game Layer
        addChild(gameLayer)
        
        let layerPosition = adjustPoint(CGPoint(
            x: -TileWidthOriginal * CGFloat(NumColumns) / 2,
            y: (TileHeightOriginal * 1)
            )
        )
        
        // Tiles Layer
        tilesLayer.position = layerPosition
        gameLayer.addChild(tilesLayer)
        
        // Lookig Feel Layer
        gameLayer.addChild(cropLayer)
        
        maskLayer.position = layerPosition
        cropLayer.maskNode = maskLayer
        
        // Smile Layer
        smilesLayer.position = layerPosition
        cropLayer.addChild(smilesLayer)
        
        SKLabelNode(fontNamed: "GillSans-BoldItalic")
        gameLayer.hidden = true
    }
    
    func clean() {
        smilesLayer.removeAllChildren()
    }
    
    func pointForColumn(column: Int, row: Int) -> CGPoint {
        return adjustPoint(CGPoint(
            x: CGFloat(column)*TileWidthOriginal + TileWidthOriginal/2,
            y: CGFloat(row)*TileHeightOriginal + TileHeightOriginal/2))
    }
    
    // Tile
    func addTiles() {
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                if let tile = level.tileAtColumn(column, row: row) {
                    let tileNode = SKSpriteNode(imageNamed: "MaskTile")
                    tileNode.size = adjustSize(tileNode.size)
                    tileNode.position = pointForColumn(column, row: row)
                    maskLayer.addChild(tileNode)
                }
            }
        }
        
        // Round Corners
        for row in 0...NumRows {
            for column in 0...NumColumns {
                let topLeft     = (column > 0) && (row < NumRows)
                    && level.tileAtColumn(column - 1, row: row) != nil
                let bottomLeft  = (column > 0) && (row > 0)
                    && level.tileAtColumn(column - 1, row: row - 1) != nil
                let topRight    = (column < NumColumns) && (row < NumRows)
                    && level.tileAtColumn(column, row: row) != nil
                let bottomRight = (column < NumColumns) && (row > 0)
                    && level.tileAtColumn(column, row: row - 1) != nil
                
                // The tiles are named from 0 to 15, according to the bitmask that is
                // made by combining these four values.
                let value = Int(topLeft) | Int(topRight) << 1 | Int(bottomLeft) << 2 | Int(bottomRight) << 3
                
                // Values 0 (no tiles), 6 and 9 (two opposite tiles) are not drawn.
                if value != 0 && value != 6 && value != 9 {
                    let name = String(format: "Tile_%ld", value)
                    let tileNode = SKSpriteNode(imageNamed: name)
                    tileNode.size = adjustSize(tileNode.size)
                    var point = pointForColumn(column, row: row)
                    point.x -= TileWidth/2
                    point.y -= TileHeight/2
                    tileNode.position = point
                    tilesLayer.addChild(tileNode)
                }
            }
        }
    }
    
    // Smile
    func addSpritesFor(smiles: Set<Smile>) {
        for smile in smiles {
            let sprite = SKSpriteNode(imageNamed: smile.smileType.spriteName)
            sprite.size = adjustSize(sprite.size)
            sprite.position = pointForColumn(smile.column, row:smile.row)
            smilesLayer.addChild(sprite)
            smile.sprite = sprite
            
            
            // Fade
            sprite.alpha = 0
            sprite.xScale = 0.5
            sprite.yScale = 0.5
            
            sprite.runAction(
                SKAction.sequence([
                    SKAction.waitForDuration(0.25, withRange: 0.5),
                    SKAction.group([
                        SKAction.fadeInWithDuration(0.25),
                        SKAction.scaleTo(1.0, duration: 0.25)
                        ])
                    ]))
        }
    }
    
    // Selection
    func showSelectionIndicatorFor(smile: Smile) {
        if selectionSprite.parent != nil {
            selectionSprite.removeFromParent()
        }
        
        selectionSprite = highlight(smile)
    }
    
    private func highlight(smile: Smile) -> SKSpriteNode {
        var selectionSprite = SKSpriteNode()
        
        if let sprite = smile.sprite {
            let texture = SKTexture(imageNamed: smile.smileType.highlightedSpriteName)
            selectionSprite.size = adjustSize(texture.size())
            selectionSprite.runAction(SKAction.setTexture(texture))
            
            sprite.addChild(selectionSprite)
            selectionSprite.alpha = 1.0
        }
        
        return selectionSprite
    }
    
    func hideSelectionIndicator() {
        selectionSprite.runAction(SKAction.sequence([
            SKAction.fadeOutWithDuration(0.3),
            SKAction.removeFromParent()]))
    }
    
    // Swipe
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        let touch = touches.first as! UITouch
        let location = touch.locationInNode(smilesLayer)
        
        let (success, column, row) = convertPoint(location)
        if success {
            if let smile = level.getSmileAt(column, row: row) {
                // Selection
                showSelectionIndicatorFor(smile)
                
                swipeFromColumn = column
                swipeFromRow = row
            }
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        if swipeFromColumn == nil { return }
        
        let touch = touches.first as! UITouch
        let location = touch.locationInNode(smilesLayer)
        
        let (success, column, row) = convertPoint(location)
        if success {
            
            var horzDelta = 0, vertDelta = 0
            if column < swipeFromColumn! {
                // swipe left
                horzDelta = -1
            }
            else if column > swipeFromColumn! {
                // swipe right
                horzDelta = 1
            } else if row < swipeFromRow! {
                // swipe down
                vertDelta = -1
            } else if row > swipeFromRow! {
                // swipe up
                vertDelta = 1
            }
            
            if horzDelta != 0 || vertDelta != 0 {
                trySwapHorizontal(horzDelta, vertical: vertDelta)
                
                // Selction
                hideSelectionIndicator()
                
                swipeFromColumn = nil
            }
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        // Selction
        if selectionSprite.parent != nil && swipeFromColumn != nil {
            hideSelectionIndicator()
        }
        
        swipeFromColumn = nil
        swipeFromRow = nil
    }
    
    override func touchesCancelled(touches: Set<NSObject>, withEvent event: UIEvent) {
        touchesEnded(touches, withEvent: event)
    }
    
    func convertPoint(point: CGPoint) -> (success: Bool, column: Int, row: Int) {
        if point.x >= 0 && point.x < CGFloat(NumColumns)*TileWidth &&
            point.y >= 0 && point.y < CGFloat(NumRows)*TileHeight {
                return (true, Int(point.x / TileWidth), Int(point.y / TileHeight))
        } else {
            // invalid location
            return (false, 0, 0)
        }
    }
    
    func trySwapHorizontal(horzDelta: Int, vertical vertDelta: Int) {
        let toColumn = swipeFromColumn! + horzDelta
        let toRow = swipeFromRow! + vertDelta
        
        if toColumn < 0 || toColumn >= NumColumns {
            return
        }
        
        if toRow < 0 || toRow >= NumRows {
            return
        }
        
        if let toSmile = level.getSmileAt(toColumn, row: toRow) {
            if let fromSmile = level.getSmileAt(swipeFromColumn!, row: swipeFromRow!) {
                if let handler = swipeHandler {
                    let swap = Swap(smile1: fromSmile, smile2: toSmile)
                    handler(swap)
                }
            }
        }
    }
    
    // Animate
    // Swipe
    func animateSwap(swap: Swap, completion: () -> ()) {
        let sprite1 = swap.smile1.sprite!
        let sprite2 = swap.smile2.sprite!
        
        sprite1.zPosition = 100
        sprite2.zPosition = 90
        
        let Duration: NSTimeInterval = 0.3
        
        let move1 = SKAction.moveTo(sprite2.position, duration: Duration)
        move1.timingMode = .EaseOut
        sprite1.runAction(move1, completion: completion)
        
        let move2 = SKAction.moveTo(sprite1.position, duration: Duration)
        move2.timingMode = .EaseOut
        sprite2.runAction(move2)
        
        // Sound
        runAction(swapSound)
    }
    
    // Invalid Swipe
    func animateInvalidSwap(swap: Swap, completion: () -> ()) {
        let sprite1 = swap.smile1.sprite!
        let sprite2 = swap.smile2.sprite!
        
        sprite1.zPosition = 100
        sprite2.zPosition = 90
        
        let Duration: NSTimeInterval = 0.2
        
        let move1 = SKAction.moveTo(sprite2.position, duration: Duration)
        move1.timingMode = .EaseOut
        
        let move2 = SKAction.moveTo(sprite1.position, duration: Duration)
        move2.timingMode = .EaseOut
        
        sprite1.runAction(SKAction.sequence([move1, move2]), completion: completion)
        sprite2.runAction(SKAction.sequence([move2, move1]))
        
        // Sound
        runAction(invalidSwapSound)
    }
    
    // Match
    func animateMatches(chains: Set<Chain>, animateScore:Bool, completion: () -> ()) {
        for chain in chains {
            
            if animateScore {
                animateScoreForChain(chain)
            }
            
            for smile in chain.smiles {
                if let sprite = smile.sprite {
                    if sprite.actionForKey("removing") == nil {
                        
                        highlight(smile)
                        
                        let scaleAction = SKAction.scaleTo(0.1, duration: 0.3)
                        scaleAction.timingMode = .EaseOut
                        sprite.runAction(SKAction.sequence([
                            SKAction.waitForDuration(0.3),
                            scaleAction,
                            SKAction.removeFromParent()
                            ]),
                            withKey:"removing")
                    }
                }
            }
        }
        
        // Sound
        runAction(matchSound)
        runAction(SKAction.waitForDuration(0.6), completion: completion)
    }
    
    // Drop
    func animateFalling(columns: [[Smile]], completion: () -> ()) {
        // 1
        var longestDuration: NSTimeInterval = 0
        for array in columns {
            for (idx, smile) in enumerate(array) {
                let newPosition = pointForColumn(smile.column, row: smile.row)
                
                let delay = 0.05 + 0.15*NSTimeInterval(idx)
                
                let sprite = smile.sprite!
                let duration = NSTimeInterval(((sprite.position.y - newPosition.y) / TileHeight) * 0.1)
                
                longestDuration = max(longestDuration, duration + delay)
                
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.runAction(
                    SKAction.sequence([
                        SKAction.waitForDuration(delay),
                        SKAction.group([moveAction, fallingSound])]))
            }
        }
        // Sound
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    // New
    func animateNew(columns: [[Smile]], completion: () -> ()) {
        
        var longestDuration: NSTimeInterval = 0
        
        for array in columns {
            
            let startRow = array[0].row + 1
            
            for (idx, smile) in enumerate(array) {
                
                let sprite = SKSpriteNode(imageNamed: smile.smileType.spriteName)
                sprite.size = adjustSize(sprite.size)
                sprite.position = pointForColumn(smile.column, row: startRow)
                smilesLayer.addChild(sprite)
                smile.sprite = sprite
                
                let delay = 0.1 + 0.2 * NSTimeInterval(array.count - idx - 1)
                
                let duration = NSTimeInterval(startRow - smile.row) * 0.1
                longestDuration = max(longestDuration, duration + delay)
                
                let newPosition = pointForColumn(smile.column, row: smile.row)
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.alpha = 0
                sprite.runAction(
                    SKAction.sequence([
                        SKAction.waitForDuration(delay),
                        SKAction.group([
                            SKAction.fadeInWithDuration(0.05),
                            moveAction,
                            addSound])
                        ]))
            }
        }
        // Sound
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    // Score
    func animateScoreForChain(chain: Chain) {
        // Figure out what the midpoint of the chain is.
        let firstSprite = chain.first().sprite!
        let lastSprite = chain.last().sprite!
        let centerPosition = adjustPoint(CGPoint(
            x: (firstSprite.position.x + lastSprite.position.x)/2,
            y: (firstSprite.position.y + lastSprite.position.y)/2 - 8))
        
        // Add a label for the score that slowly floats up.
        let scoreLabel = SKLabelNode(fontNamed: "GillSans-BoldItalic")
        scoreLabel.fontSize = 16
        scoreLabel.text = String(format: "%ld", chain.score)
        scoreLabel.position = centerPosition
        scoreLabel.zPosition = 300
        smilesLayer.addChild(scoreLabel)
        
        let moveAction = SKAction.moveBy(CGVector(dx: 0, dy: 3), duration: 0.7)
        moveAction.timingMode = .EaseOut
        scoreLabel.runAction(SKAction.sequence([moveAction, SKAction.removeFromParent()]))
    }
    
    // Game Over
    func animateGameOver(completion: () -> ()) {
        let action = SKAction.moveBy(CGVector(dx: 0, dy: -size.height), duration: 0.3)
        action.timingMode = .EaseIn
        gameLayer.runAction(action, completion: completion)
    }
    
    // New Game
    func animateBeginGame(completion: () -> ()) {
        gameLayer.hidden = false
        gameLayer.position = CGPoint(x: 0, y: -size.height)
        let action = SKAction.moveBy(CGVector(dx: 0, dy: size.height), duration: 0.3)
        action.timingMode = .EaseOut
        gameLayer.runAction(action, completion: completion)
    }
}