//
//  Swap.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import Foundation

struct Swap: Printable, Hashable {
    let smile1: Smile
    let smile2: Smile
    
    init(smile1: Smile, smile2: Smile) {
        self.smile1 = smile1
        self.smile2 = smile2
    }
    
    var description: String {
        return "swap \(smile1) with \(smile2)"
    }
    
    var hashValue: Int {
        return smile1.hashValue ^ smile2.hashValue
    }
}

func ==(lhs: Swap, rhs: Swap) -> Bool {
    return (lhs.smile1 == rhs.smile1 && lhs.smile2 == rhs.smile2) ||
        (lhs.smile2 == rhs.smile1 && lhs.smile1 == rhs.smile2)
}