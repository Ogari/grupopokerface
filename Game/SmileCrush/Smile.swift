//
//  Smile.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import SpriteKit

class Smile: Printable, Hashable {
    var column: Int
    var row: Int
    let smileType: SmileType
    var sprite: SKSpriteNode?
    
    init(column: Int, row: Int, smileType: SmileType) {
        self.column = column
        self.row = row
        self.smileType = smileType
    }
    
    var description: String {
        return "type:\(smileType) square:(\(column),\(row))"
    }
    
    var hashValue: Int {
        return row*10 + column
    }
}

enum SmileType: Int, Printable {
    case Unknown = 0, Blue, Red, Yellow, Purple, Green, PowerUpBomb
    
    var spriteName: String {
        let spriteNames = [
            "blue",
            "red",
            "yellow",
            "purple",
            "green",
            "powerup-bomb"
        ]
        
        return spriteNames[rawValue - 1]
    }
    
    var highlightedSpriteName: String {
        return spriteName + "_high"
    }
    
    static func random() -> SmileType {
        return SmileType(rawValue: Int(arc4random_uniform(5)) + 1)!
    }
    
    var description: String {
        return spriteName
    }
}

func ==(lhs: Smile, rhs: Smile) -> Bool {
    return lhs.column == rhs.column && lhs.row == rhs.row
}
