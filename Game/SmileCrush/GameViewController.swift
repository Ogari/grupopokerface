//
//  GameViewController.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

import UIKit
import SpriteKit

struct GameOptions {
    var targetScore:Int = -1
    var maximumMoves:Int = -1
    var maximumTime:Int = 0
    var gameSmiles:[SmileType]? = nil
    var players:Int = 1
    var level:Int = 0
}

class GameViewController: UIViewController {
    var scene: GameScene!
    
    var level: Level!
    
    var players = [Player]()
    var currentPlayerIdx = 0
    
    var gameOptions:GameOptions!
    
    @IBOutlet weak var player1ScoreLabel: UILabel!
    
    @IBOutlet weak var target: UILabel!
    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var player2Container: UIView!
    @IBOutlet weak var player2ScoreLabel: UILabel!
    
    @IBOutlet weak var gameOverPanel: UIImageView!
    @IBOutlet weak var shuffleButton: UIButton!
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    // Sound
    lazy var backgroundMusic: AVAudioPlayer = {
        let url = NSBundle.mainBundle().URLForResource("Mining by Moonlight", withExtension: "mp3")
        let audioPlayer = AVAudioPlayer(contentsOfURL: url, error: nil)
        audioPlayer.numberOfLoops = -1
        return audioPlayer
        }()
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
    }
    // Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Change Target Label
        if gameOptions.targetScore != -1 {
            self.target.text = "Target Score:"
        }
        else {
            self.target.text = "Moves Left:"
        }
        
        // Hide Player 2 Container
        if gameOptions.players == 1 {
            self.player2Container.hidden = true
        }
        else {
            self.player2Container.hidden = false
        }
        
        // Mock
        self.shuffleButton.hidden = true
        
        // Configure the view.
        let skView = view as! SKView
        skView.multipleTouchEnabled = false
        
        // Create and configure the scene.
        scene = GameScene(size: skView.bounds.size)
        scene.scaleMode = .AspectFill
        scene.backgroundColor = UIColor.whiteColor()
        
        // Present the scene.
        skView.presentScene(scene)
        
        // Load level
        level = Level(filename: String(format: "Level_%d", arguments: [gameOptions.level]))
        scene.level = level
        
        // Load Tile
        scene.addTiles()
        
        // Swipe Handler
        scene.swipeHandler = handleSwipe
        
        beginGame()
        
        gameOverPanel.hidden = true
        
        backgroundMusic.play()
    }
    
    // Game
    func beginGame() {
        players.removeAll(keepCapacity: false)
        
        for var i = 0; i <= gameOptions.players; i++ {
            var player = Player(name: String(format: "Player%d", arguments: [i + 1]))
            player.movesLeft = gameOptions.maximumMoves
            players.append(player)
        }
        
        currentPlayerIdx = Int(arc4random_uniform(UInt32(players.count)))
        currentPlayerIdx = 0
        
        updateLabels()
        
        level.resetComboMultiplier()
        
        scene.animateBeginGame() {
//            self.shuffleButton.hidden = false
        }
        
        shuffle()
    }
    
    func shuffle() {
        scene.clean()
        
        let smiles = level.shuffle()
        scene.addSpritesFor(smiles)
    }
    
    func beginNextTurn() {
        level.resetComboMultiplier()
        level.detectPossibleSwaps()
        view.userInteractionEnabled = true
        
        
        if (gameOptions.maximumMoves != -1) {
            decrementMoves()
        }
        checkEndGame()
        
        currentPlayerIdx = ++currentPlayerIdx % (players.count - 1)
        updateLabels()
    }

    // MARK-: Updating score labels
    
    func updateLabels() {
            
        if gameOptions.targetScore != -1 {
            targetLabel.text = String(format: "%ld", gameOptions.targetScore)
        }
        else if gameOptions.maximumMoves != -1 {
            targetLabel.text = String(format: "%ld", players[currentPlayerIdx].movesLeft)
        }
        
        player1ScoreLabel.text = String(format: "%ld", players[0].score)
        player2ScoreLabel.text = String(format: "%ld", players[1].score)
        
        if (currentPlayerIdx == 0) {
            player1ScoreLabel.backgroundColor = UIColor.blackColor()
            player2ScoreLabel.backgroundColor = UIColor(red: 229/255, green: 0/255, blue: 17/255, alpha: 1.0)
        }
        else {
            player1ScoreLabel.backgroundColor = UIColor(red: 0/255, green: 59/255, blue: 253/255, alpha: 0.2)
            player2ScoreLabel.backgroundColor = UIColor.blackColor()
        }
    }
    
    func checkEndGame() {
        if players[currentPlayerIdx].score >= gameOptions.targetScore && gameOptions.targetScore != -1 {
            gameOverPanel.image = UIImage(named: String(format: "Player%ldWin", currentPlayerIdx + 1))
            showGameOver()
        } else if players[currentPlayerIdx].movesLeft == 0 && players[(currentPlayerIdx + 1) % players.count].movesLeft == 0 && gameOptions.maximumMoves != -1 {
            
            var pWin = players[0].score > players[1].score ? 1 : 2
            gameOverPanel.image = UIImage(named: String(format: "Player%ldWin", pWin))
            showGameOver()
        }
        else if gameOptions == nil {
            gameOverPanel.image = UIImage(named: "GameOver")
        }
    }
    
    // Move
    func decrementMoves() {
        --players[currentPlayerIdx].movesLeft
//        updateLabels()
    }
    
    // Swipe
    func handleSwipe(swap: Swap) {
        view.userInteractionEnabled = false
        
        if level.isPossibleSwap(swap) {
            level.performSwap(swap)
            scene.animateSwap(swap, completion: handleMatches)
        } else {
            scene.animateInvalidSwap(swap) {
                self.view.userInteractionEnabled = true
            }
        }
    }
    
    // Match
    func handleMatches() {
        let chains = level.removeMatches()
        
        if chains.count == 0 {
            beginNextTurn()
            return
        }
        
        var animateScore = gameOptions.gameSmiles == nil ? true : false
        scene.animateMatches(chains, animateScore: animateScore) {
            
            // Score
            for chain in chains {
                if self.gameOptions.gameSmiles == nil {
                    self.players[self.currentPlayerIdx].score += chain.score
                }
                else if chain.smiles[0].smileType == self.gameOptions.gameSmiles?[self.currentPlayerIdx] {
                    self.players[self.currentPlayerIdx].score += chain.length
                }
            }
            self.updateLabels()
            
            let columns = self.level.fillHoles()
            self.scene.animateFalling(columns) {
                let columns = self.level.newTopSmiles()
                self.scene.animateNew(columns) {
                    self.handleMatches()
                }
            }
        }
    }
    
    // Game Over
    func showGameOver() {
        gameOverPanel.hidden = false
        scene.userInteractionEnabled = false
        
        scene.animateGameOver() {
            self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideGameOver")
            self.view.addGestureRecognizer(self.tapGestureRecognizer)
        }
        
        shuffleButton.hidden = true
    }
    
    func hideGameOver() {
        view.removeGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer = nil
        
        gameOverPanel.hidden = true
        scene.userInteractionEnabled = true
        
        beginGame()
    }
    
    // Shuffle Button
    @IBAction func shuffleButtonPressed(AnyObject) {
        shuffle()
        decrementMoves()
        
        checkEndGame()
    }
}