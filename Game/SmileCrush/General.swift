//
//  General.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 26/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import Foundation
import UIKit

func adjustX(x:CGFloat)->CGFloat {
    let bounds = UIScreen.mainScreen().bounds.size
    return x*(bounds.width/375)
}

func adjustY(y:CGFloat)->CGFloat {
    let bounds = UIScreen.mainScreen().bounds.size
    
    return y*(bounds.height/667)
}

func adjustPoint(originalPoint:CGPoint)->CGPoint {
    let bounds = UIScreen.mainScreen().bounds.size
    
    return CGPoint(x: originalPoint.x*(bounds.width/375), y: originalPoint.y*(bounds.height/667))
}

func adjustSize(originalSize:CGSize)->CGSize {
    let bounds = UIScreen.mainScreen().bounds.size
    
    return CGSize(width: originalSize.width*(bounds.width/375), height: originalSize.height*(bounds.height/667))
}

func adjustRectSize(originalRectangle:CGRect)->CGRect {
    
    let bounds = UIScreen.mainScreen().bounds.size
    
    return CGRect(x: originalRectangle.origin.x*(bounds.width/375),
        y: originalRectangle.origin.y*(bounds.height/667),
        width: originalRectangle.size.width*(bounds.width/375),
        height: originalRectangle.size.height*(bounds.height/667))
}