//
//  MultiMovesTableViewController.swift
//  SmileCrush
//
//  Created by Thiago Felipe Alves on 31/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import UIKit

class MultiPlayerModeViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "MultiMove") {
            var vc = segue.destinationViewController as! GameViewController;
            
            vc.gameOptions = GameOptions()
            vc.gameOptions.maximumMoves = 20 // Valor que pode ser alterado
            vc.gameOptions.players = 2
            
        }else if (segue.identifier == "MultiScore"){
            var vc = segue.destinationViewController as! GameViewController;
            
            vc.gameOptions = GameOptions()
            vc.gameOptions.targetScore = 1000 // Valor que pode ser alterado
            vc.gameOptions.players = 2
            
        }else if (segue.identifier == "MultiSmile"){
            var vc = segue.destinationViewController as! GameViewController;
            
            vc.gameOptions = GameOptions()
            vc.gameOptions.targetScore = 50 // Valor que pode ser alterado
            vc.gameOptions.players = 2
            vc.gameOptions.gameSmiles = [SmileType.Blue, SmileType.Red] // Valor que pode ser alterado
        }
    }
    
    // MARK: - Table view data source

    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 0
    }*/

}
