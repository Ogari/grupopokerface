//
//  Single_Table.swift
//  SmileCrush
//
//  Created by Thiago Felipe Alves on 31/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import UIKit

class SinglePlayerModeViewController: UITableViewController {

    var toPass:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "SingleMove") {
            var vc = segue.destinationViewController as! GameViewController;
            
            vc.gameOptions = GameOptions()
            vc.gameOptions.maximumMoves = 20 // Valor que pode ser alterado
            vc.gameOptions.players = 1
        }
    }
    
    // MARK: - Table view data source
    
    /*override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 0
    }*/

}
