//
//  Player.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 26/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import Foundation

class Player {
    var name:String
    var score:Int = 0
    var movesLeft:Int = 0
    
    convenience init () {
        self.init(name: "")
    }
    
    init(name:String) {
        self.name = name
    }
}