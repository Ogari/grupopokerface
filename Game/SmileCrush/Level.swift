//
//  Level.swift
//  SmileCrush
//
//  Created by Gabriel Maciel Bueno Luna Freire on 24/08/15.
//  Copyright (c) 2015 Gabriel Maciel Bueno Luna Freire. All rights reserved.
//

import Foundation

let NumColumns = 9
let NumRows = 9

class Level {
    
    private var smiles = Array2D<Smile>(columns: NumColumns, rows: NumRows)
    private var tiles = Array2D<Tile>(columns: NumColumns, rows: NumRows)
    
    private var possibleSwaps = Set<Swap>()
    
    private var comboMultiplier = 0
    
    init(filename: String) {
        if let dictionary = Dictionary<String, AnyObject>.loadJSONFromBundle(filename) {
            if let tilesArray: AnyObject = dictionary["tiles"] {
                for (row, rowArray) in enumerate(tilesArray as! [[Int]]) {
                    let tileRow = NumRows - row - 1
                    for (column, value) in enumerate(rowArray) {
                        if value == 1 {
                            tiles[column, tileRow] = Tile()
                        }
                    }
                }
            }
        }
    }
    
    // Get Objects
    func tileAtColumn(column: Int, row: Int) -> Tile? {
        assert(column >= 0 && column < NumColumns)
        assert(row >= 0 && row < NumRows)
        return tiles[column, row]
    }
    
    func getSmileAt(column: Int, row: Int) -> Smile? {
        assert(column >= 0 && column < NumColumns)
        assert(row >= 0 && row < NumRows)
        return smiles[column, row]
    }
    
    // Game
    func shuffle() -> Set<Smile> {
        var set: Set<Smile>
        do {
            set = startLevel()
            detectPossibleSwaps()
//            println("possible swaps: \(possibleSwaps)")
        }
            while possibleSwaps.count == 0
        
        return set
    }
    
    private func startLevel() -> Set<Smile> {
        var set = Set<Smile>()
        
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                
                if tiles[column, row] != nil {
                    
                    // Validate 3 spites in a row or a column equals
                    var smileType: SmileType
                    do {
                        smileType = SmileType.random()
                    }
                    while (column >= 2 &&
                        smiles[column - 1, row]?.smileType == smileType &&
                        smiles[column - 2, row]?.smileType == smileType)
                        ||
                        (row >= 2 &&
                        smiles[column, row - 1]?.smileType == smileType &&
                        smiles[column, row - 2]?.smileType == smileType)
                    
                    let smile = Smile(column: column, row: row, smileType: smileType)
                    smiles[column, row] = smile
                    set.insert(smile)
                }
            }
        }
        return set
    }
    
    // Game Score
    private func calculateScores(chains: Set<Chain>) {
        // 3-chain is 60 pts, 4-chain is 120, 5-chain is 180, and so on
        for chain in chains {
            chain.score = 60 * (chain.length - 2) * comboMultiplier
            ++comboMultiplier
        }
    }
    
    func resetComboMultiplier() {
        comboMultiplier = 1
    }
    
    // Swipe
    func performSwap(swap: Swap) {
        let column1 = swap.smile1.column
        let row1 = swap.smile1.row
        let column2 = swap.smile2.column
        let row2 = swap.smile2.row
        
        smiles[column1, row1] = swap.smile2
        swap.smile2.column = column1
        swap.smile2.row = row1
        
        smiles[column2, row2] = swap.smile1
        swap.smile1.column = column2
        swap.smile1.row = row2
    }
    
    func isPossibleSwap(swap: Swap) -> Bool {
        return possibleSwaps.contains(swap)
    }
    
    func detectPossibleSwaps() {
        var set = Set<Swap>()
        
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                if let smile = smiles[column, row] {
                    
                    // Bomb can move to any side with smile
                    if smile.smileType == SmileType.PowerUpBomb {
                        
                        //right
                        if column < NumColumns - 1 {
                            if let other = smiles[column + 1, row] {
                                set.insert(Swap(smile1: smile, smile2: other))
                            }
                        }
                        //left
                        if column > 0 {
                            if let other = smiles[column - 1, row] {
                                set.insert(Swap(smile1: smile, smile2: other))
                            }
                        }
                        //bottom
                        if row > 0 {
                            if let other = smiles[column, row - 1] {
                                set.insert(Swap(smile1: smile, smile2: other))
                            }
                        }
                        //top
                        if row < NumRows - 1 {
                            if let other = smiles[column, row + 1] {
                                set.insert(Swap(smile1: smile, smile2: other))
                            }
                        }
                    }
                    else {
                        // Right
                        if column < NumColumns - 1 {
                            if let other = smiles[column + 1, row] {
                                // Swipe test
                                smiles[column, row] = other
                                smiles[column + 1, row] = smile
                            
                                // Match?
                                if hasChainAtColumn(column + 1, row: row) ||
                                    hasChainAtColumn(column, row: row) {
                                        set.insert(Swap(smile1: smile, smile2: other))
                                }
                            
                                // Swipe back
                                smiles[column, row] = smile
                                smiles[column + 1, row] = other
                            }
                        }
                    
                        // Top
                        if row < NumRows - 1 {
                            if let other = smiles[column, row + 1] {
                                // Swipe test
                                smiles[column, row] = other
                                smiles[column, row + 1] = smile
                            
                                // Is either cookie now part of a chain?
                                if hasChainAtColumn(column, row: row + 1) ||
                                    hasChainAtColumn(column, row: row) {
                                        set.insert(Swap(smile1: smile, smile2: other))
                                }
                            
                                // Swipe back
                                smiles[column, row] = smile
                                smiles[column, row + 1] = other
                            }
                        }
                    }
                }
            }
        }
        
        possibleSwaps = set
    }
    
    // Chain
    private func hasChainAtColumn(column: Int, row: Int) -> Bool {
        let smileType = smiles[column, row]!.smileType
        
        if smileType == SmileType.PowerUpBomb {
            return false
        }
        
        var horzLength = 1
        //left
        for var i = column - 1;
            i >= 0 && smiles[i, row]?.smileType == smileType;
            --i, ++horzLength {
        }
        //right
        for var i = column + 1;
            i < NumColumns && smiles[i, row]?.smileType == smileType;
            ++i, ++horzLength {
        }
        
        if horzLength >= 3 {
            return true
        }
        
        var vertLength = 1
        //bottom
        for var i = row - 1;
            i >= 0 && smiles[column, i]?.smileType == smileType;
            --i, ++vertLength {
        }
        //top
        for var i = row + 1;
            i < NumRows && smiles[column, i]?.smileType == smileType;
            ++i, ++vertLength {
        }
        
        return vertLength >= 3
    }
    
    // Matches
    private func detectHorizontalMatches() -> Set<Chain> {
        var set = Set<Chain>()

        for row in 0..<NumRows {
            for var column = 0; column < NumColumns - 2 ; {

                if let smile = smiles[column, row] {
                    
                    let matchType = smile.smileType
                    
                    if matchType == SmileType.PowerUpBomb {
                        ++column
                        continue
                    }

                    if smiles[column + 1, row]?.smileType == matchType &&
                        smiles[column + 2, row]?.smileType == matchType {
                            
                            let chain = Chain(chainType: .Horizontal)
                            do {
                                chain.add(smiles[column, row]!)
                                ++column
                            }
                            while column < NumColumns && smiles[column, row]?.smileType == matchType
                            
                            set.insert(chain)
                            continue
                    }
                }
                ++column
            }
        }
        return set
    }
    
    private func detectVerticalMatches() -> Set<Chain> {
        var set = Set<Chain>()
        
        for column in 0..<NumColumns {
            for var row = 0; row < NumRows - 2; {
                if let smile = smiles[column, row] {
                    let matchType = smile.smileType
                    
                    if matchType == SmileType.PowerUpBomb {
                        ++row
                        continue
                    }
                    
                    if smiles[column, row + 1]?.smileType == matchType &&
                        smiles[column, row + 2]?.smileType == matchType {
                            
                            let chain = Chain(chainType: .Vertical)
                            do {
                                chain.add(smiles[column, row]!)
                                ++row
                            }
                            while row < NumRows && smiles[column, row]?.smileType == matchType
                            
                            set.insert(chain)
                            continue
                    }
                }
                ++row
            }
        }
        return set
    }
    
    // Remove
    func removeMatches() -> Set<Chain> {
        let horizontalChains = detectHorizontalMatches()
        let verticalChains = detectVerticalMatches()
        
        remove(horizontalChains)
        remove(verticalChains)
        
        // Score
        calculateScores(horizontalChains)
        calculateScores(verticalChains)
        
        return horizontalChains.union(verticalChains)
    }
    
    private func remove(chains: Set<Chain>) {
        for chain in chains {
            for smile in chain.smiles {
                smiles[smile.column, smile.row] = nil
            }
//            if (chain.length == 5) {
//                let midSmile = chain.smiles[2]
//                smiles[midSmile.column, midSmile.row] = Smile(column: midSmile.column, row: midSmile.row, smileType: SmileType.PowerUpBomb)
//            }
        }
    }
    
    // Drop
    func fillHoles() -> [[Smile]] {
        var columns = [[Smile]]()
        
        for column in 0..<NumColumns {
            var array = [Smile]()
            
            for row in 0..<NumRows {
                if tiles[column, row] != nil && smiles[column, row] == nil {
                    for lookup in (row + 1)..<NumRows {
                        if let smile = smiles[column, lookup] {
                            
                            smiles[column, lookup] = nil
                            smiles[column, row] = smile
                            smile.row = row
                            
                            array.append(smile)
                            
                            break
                        }
                    }
                }
            }
            
            if !array.isEmpty {
                columns.append(array)
            }
        }
        return columns
    }
    
    func newTopSmiles() -> [[Smile]] {
        var columns = [[Smile]]()
        var smileType: SmileType = .Unknown
        
        for column in 0..<NumColumns {
            var array = [Smile]()
            
            for var row = NumRows - 1; row >= 0 && smiles[column, row] == nil; --row {
                
                if tiles[column, row] != nil {
                    
                    var newSmileType: SmileType
                    do {
                        newSmileType = SmileType.random()
                    } while newSmileType == smileType
                    smileType = newSmileType
                    
                    let smile = Smile(column: column, row: row, smileType: smileType)
                    smiles[column, row] = smile
                    array.append(smile)
                }
            }
            
            if !array.isEmpty {
                columns.append(array)
            }
        }
        return columns
    }
}